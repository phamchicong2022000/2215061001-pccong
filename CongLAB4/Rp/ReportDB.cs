﻿using CongLAB4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CongLAB4.Rp
{
    internal class ReportDB
    {
        public string StudentID { get; set; }

        public string FullName { get; set; }

        public double AverageScore { get; set; }

        public String FacultyName { get; set; }

    }
}
