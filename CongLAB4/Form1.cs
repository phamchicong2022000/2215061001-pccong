﻿using CongLAB4.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CongLAB4
{
    public partial class Form1 : Form
    {

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Studentcn context = new Studentcn();
                List<Faculty> listFalcultys = context.Faculties.ToList();
                List<Student> listStudent = context.Students.ToList();
                FillFalcultyCombobox(listFalcultys);
                BindGrid(listStudent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            this.cbchuyennganh.DataSource = listFalcultys;
            this.cbchuyennganh.DisplayMember = "FacultyName";
            this.cbchuyennganh.ValueMember = "FacultyID";
        }
        private void BindGrid(List<Student> listStudent)
        {
            dgsv.Rows.Clear();
            foreach (var item in listStudent)
            {
                int index = dgsv.Rows.Add();
                dgsv.Rows[index].Cells["masv"].Value = item.StudentID;
                dgsv.Rows[index].Cells["hovaten"].Value = item.FullName;
                dgsv.Rows[index].Cells["khoa"].Value = item.Faculty.FacultyName;
                dgsv.Rows[index].Cells["diemtb"].Value = item.AverageScore;
            }
        }

        private void btnthem_Click(object sender, EventArgs e)
        {
            var txtmsvValue  = txtmsv.Text;
            var txhovatenValue  = txthovaten.Text;
            var cmbkhoaValue  = cbchuyennganh.SelectedValue;
            var txtdiemtbValue  = txtDiemTB.Text;
            if(txtmsvValue == "")
            {
                MessageBox.Show("Ma so sinh vien ko dc de trong");
                return;
            }
            if(txhovatenValue == "")
            {

                MessageBox.Show("Ten sinh vien ko dc de trong");
                return;
            }
            try
            {
                Student student = new Student();
                student.StudentID = txtmsvValue;
                student.FullName = txhovatenValue;
                student.FacultyID = (int)cmbkhoaValue;
                student.AverageScore = double.Parse(txtdiemtbValue);

                Studentcn db = new Studentcn();
                db.Students.Add(student);
                db.SaveChanges();
                BindGrid(db.Students.ToList());
                clear();
            }catch (Exception ex)
            {
                MessageBox.Show("doi lai mssv");
            }
       
           
        }
        private void clear()
        {
            txtmsv.Clear();
            txthovaten.Clear();
            txtDiemTB.Clear();
            cbchuyennganh.SelectedIndex = 0;

        }

        private void dgsv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridViewRow row = new DataGridViewRow();
            row = dgsv.Rows[e.RowIndex];
            txtmsv.Text = row.Cells["masv"].Value.ToString();
            txthovaten.Text = row.Cells["hovaten"].Value.ToString();
            txtDiemTB.Text = row.Cells["diemtb"].Value.ToString();
            cbchuyennganh.Text = row.Cells["khoa"].Value.ToString();
            
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            DialogResult xx = MessageBox.Show("Có chắc muốn xóa?", "Xác nhận", MessageBoxButtons.OKCancel);
            if (xx == DialogResult.OK)
            {
                Studentcn context = new Studentcn();
                var txtmsvValue = txtmsv.Text;
                Student dbDelete = context.Students.SingleOrDefault(x => x.StudentID == txtmsvValue);

                if (dbDelete != null)
                {
                    context.Students.Remove(dbDelete);
                    context.SaveChanges();
                    BindGrid(context.Students.ToList());
                    clear();
                }
            }
            
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            Studentcn context = new Studentcn();
            var txtmsvValue = txtmsv.Text;
            Student dbsua = context.Students.SingleOrDefault(x => x.StudentID == txtmsvValue);
            if(dbsua == null) {
                MessageBox.Show("KO tim thay sinh vien co ma so" + txtmsvValue);

            }
              else
            {
                dbsua.FullName= txthovaten.Text;
                dbsua.FacultyID = (int)cbchuyennganh.SelectedValue;
                dbsua.AverageScore = double.Parse(txtDiemTB.Text);
                context.SaveChanges();
                BindGrid(context.Students.ToList());
                clear();

            }
        }

        private void dgsv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
