﻿using CongLAB4.Models;
using CongLAB4.Rp;
using Microsoft.Reporting.WinForms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LAB4
{
    public partial class R : Form
    {
        public R()
        {
            InitializeComponent();
        }

        private void Ra(object sender, EventArgs e)
        {

            this.reportViewer.RefreshReport();
        }

        private void Viewer_Load(object sender, EventArgs e)
        {
            Studentcn db = new Studentcn();
            var listDB = db.Students.Select(c => new ReportDB
            {
                StudentID = c.StudentID,
                FullName = c.FullName,
                AverageScore = c.AverageScore,
                FacultyName = c.Faculty.FacultyName,
            }).ToList() ;
            this.reportViewer.LocalReport.ReportPath = "RSt.rdlc"; 
            var reportDataSource= new ReportDataSource("ST", listDB);
            this.reportViewer.LocalReport.DataSources.Clear();
            this.reportViewer.LocalReport.DataSources.Add(reportDataSource);
            this.reportViewer.RefreshReport();
        }
    }
}
